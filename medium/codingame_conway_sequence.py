# read the input
line = [input()]
l = int(input())

for i in range(1, l):
    # add one charactere to avoid out of bound reading
    line.append(' ')

    new_line = []
    previous_c = line[0]
    count_c = 0

    # read the line to determine the next one
    for c in line:
        if c != previous_c:
            new_line.extend([str(count_c), str(previous_c)])
            previous_c = c
            count_c = 1
        else:
            count_c += 1

    # replace the line by the new one
    line = new_line.copy()

# print the result
print(' '.join(line))
