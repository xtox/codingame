# the winner take all the cards in the arena
def turn_conclusion(deck):
    deck.extend(arena_1.copy())
    deck.extend(arena_2.copy())


# read the input
rank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
deck_1 = [rank.index(input()[:-1]) for i in range(int(input()))]  # deck of the player 1
deck_2 = [rank.index(input()[:-1]) for i in range(int(input()))]  # deck of the player 2

nb_turn = 0
while deck_1 and deck_2:
    nb_turn += 1
    result_ok = False

    # each player put a card on the arena
    arena_1 = [deck_1.pop(0)]
    arena_2 = [deck_2.pop(0)]

    while not result_ok:
        # player 1 wins
        if arena_1[-1] > arena_2[-1]:
            turn_conclusion(deck_1)
            result_ok = True
        # player 2 wins
        elif arena_1[-1] < arena_2[-1]:
            turn_conclusion(deck_2)
            result_ok = True
        # battle
        else:
            # ex-aequo
            if len(deck_1) < 4 or len(deck_2) < 4:
                print('PAT')
                exit()
            # perform the battle
            arena_1.extend([deck_1.pop(0), deck_1.pop(0), deck_1.pop(0), deck_1.pop(0)])
            arena_2.extend([deck_2.pop(0), deck_2.pop(0), deck_2.pop(0), deck_2.pop(0)])

print(f'1 {nb_turn}') if deck_1 else print(f'2 {nb_turn}')
