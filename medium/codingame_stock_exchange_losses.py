n = int(input())
v = [int(i) for i in input().split()]
loss_max = local_max = 0

# for each value, define the max loos and if it is a local max
for e in v:
    loss_max = min(loss_max, e - local_max)
    local_max = max(local_max, e)

print(loss_max)
