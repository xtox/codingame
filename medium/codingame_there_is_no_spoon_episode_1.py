width = int(input())  # the number of cells on the X axis
height = int(input())  # the number of cells on the Y axis
lines = [list(input()) for i in range(height)]  # the grid

# print(lines)
for i in range(height):
    for j in range(width):
        # find a node
        if lines[i][j] == '0':
            x1, y1 = j, i
            x2, y2 = -1, -1
            x3, y3 = -1, -1

            # find the node on the right
            for k in range(j+1, width):
                if lines[i][k] == '0':
                    x2, y2 = k, i
                    break

            # find the node below
            for k in range(i+1, height):
                if lines[k][j] == '0':
                    x3, y3 = j, k
                    break

            # print the result
            print(f'{x1} {y1} {x2} {y2} {x3} {y3}')
        else:
            continue
