# read the input
w, h = [int(i) for i in input().split()]
lines = [input().split() for i in range(h)]
ex = int(input())

# mapping table
tiles = {'1' : {'TOP'  : 'BOTTOM',
                'LEFT' : 'BOTTOM',
                'RIGHT': 'BOTTOM'},
         '2' : {'LEFT' : 'RIGHT',
                'RIGHT': 'LEFT'},
         '3' : {'TOP'  : 'BOTTOM'},
         '4' : {'TOP'  : 'LEFT',
                'RIGHT': 'BOTTOM'},
         '5' : {'TOP'  : 'RIGHT',
                'LEFT' : 'BOTTOM'},
         '6' : {'LEFT' : 'RIGHT',
                'RIGHT': 'LEFT'},
         '7' : {'TOP'  : 'BOTTOM',
                'RIGHT': 'BOTTOM'},
         '8' : {'LEFT' : 'BOTTOM',
                'RIGHT': 'BOTTOM'},
         '9' : {'TOP'  : 'BOTTOM',
                'LEFT' : 'BOTTOM'},
         '10': {'TOP'  : 'LEFT'},
         '11': {'TOP'  : 'RIGHT'},
         '12': {'RIGHT': 'BOTTOM'},
         '13': {'LEFT' : 'BOTTOM'}}

# game loop
while True:
    xi, yi, pos = input().split()
    xi = int(xi)
    yi = int(yi)

    # determine the type of tile
    pos_end = tiles[lines[yi][xi]][pos]

    # determine the end position
    if pos_end == 'BOTTOM':
        yi += 1
    elif pos_end == 'LEFT':
        xi -= 1
    else:
        xi += 1

    # print the result
    print(f'{xi} {yi}')
