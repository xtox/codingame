# nb_floors: number of floors
# width: width of the area
# nb_rounds: maximum number of rounds
# exit_floor: floor on which the exit is found
# exit_pos: position of the exit on its floor
# nb_total_clones: number of generated clones
# nb_additional_elevators: ignore (always zero)
# nb_elevators: number of elevators
# elevators: containt a list of lists with elevator floor and elevator position
nb_floors, width, nb_rounds, exit_floor, exit_pos, nb_total_clones, nb_additional_elevators, nb_elevators = [int(i) for i in input().split()]
elevators = dict(input().split() for i in range(nb_elevators))

# game loop
while True:
    # clone_floor: floor of the leading clone
    # clone_pos: position of the leading clone on its floor
    # direction: direction of the leading clone: LEFT or RIGHT
    clone_floor, clone_pos, clone_dir = input().split()
    clone_floor = int(clone_floor)
    clone_pos = int(clone_pos)

    # define the target position regarding the floor
    target_pos = exit_pos if clone_floor == exit_floor else int(elevators.get(str(clone_floor), '0'))

    # define the action
    if target_pos > clone_pos and clone_dir == 'LEFT' or target_pos < clone_pos and clone_dir == 'RIGHT':
        print('BLOCK')
    else:
        print('WAIT')
