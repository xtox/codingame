# w: width of the building.
# h: height of the building.
w, h = [int(i) for i in input().split()]
n = int(input())  # maximum number of turns before game over.
x0, y0 = [int(i) for i in input().split()]

# area of possiblilities
x1, x2 = 0, w - 1
y1, y2 = 0, h - 1

# game loop
while True:
    bomb_dir = input()  # the direction of the bombs

    # new area of possibilities
    if 'D' in bomb_dir:
        y1 = y0 + 1
    elif 'U' in bomb_dir:
        y2 = y0 - 1

    if 'R' in bomb_dir:
        x1 = x0 + 1
    elif 'L' in bomb_dir:
        x2 = x0 - 1

    # jump
    x0 = (x1 + x2) // 2
    y0 = (y1 + y2) // 2
    print(f'{x0} {y0}')
