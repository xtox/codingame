n = int(input())

# create the elementary triangle
e_triangle = [' ' * (n-i-1) + '*' * (2*i+1) + ' ' * (n-i-1) for i in range(n)]

# create the triforce
triforce = ''
for i in range(n):
    triforce += (' ' * n + e_triangle[i]).rstrip() + '\n'
for i in range(n):
    triforce += (e_triangle[i] + ' ' + e_triangle[i]).rstrip() + '\n'

# replace the first character by a point '.' and remove the last  '\n'
triforce = triforce[:-1].replace(' ', '.', 1)

# print the result
print(triforce)
