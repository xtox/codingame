# read the input
n = int(input())
pi = [int(input()) for i in range(n)]
delta = 10000000

# power list sort
pi.sort()

# save the lowest delta
for i in range(1, n):
    if pi[i] - pi[i-1] < delta:
        delta = pi[i] - pi[i-1]

print(delta)
