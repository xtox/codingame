w, h = [int(i) for i in input().split()]

tab  = []

# transform the graph in a more readable version
for i in range(h):
    line = ' ' + input() + ' '
    subtab = []
    for j in range((w + 2) // 3):
        e = line[3 * j: 3 * (j + 1)]
        if  e == ' | ':
            subtab.append(0)
        elif e == '-| ':
            subtab.append(-1)
        elif e == ' |-':
            subtab.append(1)
        else:
            subtab.append(e.replace(' ', ''))

    tab.append(subtab.copy())

result_dict = {}
# find the path for each column
for i in range((w + 2) // 3):
    column = i
    for j in range(h):
        if j == 0:
            result_dict[tab[0][i]] = ''
            continue
        if j == h-1:
            result_dict[tab[0][i]] = tab[j][column]
            continue
        column += int(tab[j][column])

# print the result
for k, v in result_dict.items():
    print(f'{k}{v}')        
