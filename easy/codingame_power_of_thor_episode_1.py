# read the initialization input
light_x, light_y, initial_tx, initial_ty = [int(i) for i in input().split()]

current_tx, current_ty = initial_tx, initial_ty

# game loop
while True:
    remaining_turns = int(input())  # The remaining amount of turns Thor can move. Do not remove this line.
    direction = ''

    # choose the proper direction
    if current_ty > light_y:
        direction += 'N'
        current_ty -= 1
    elif current_ty < light_y:
        direction += 'S'
        current_ty += 1

    if current_tx > light_x:
        direction += 'W'
        current_tx -= 1
    elif current_tx < light_x:
        direction += 'E'
        current_tx += 1

    # direction to follow
    print(direction)
