# read the input
string_message = input()

# convert into a list of 1 and 0
binary_message = ''.join(format(ord(c), 'b').rjust(7,'0') for c in string_message)

# convert into Chuck Norris unary
unary_message = ''
previous_byte = -1
for b in binary_message:
    if b == previous_byte:
        unary_message += '0'
    else:
        if b == '1':
            unary_message += ' 0 0'
        else:
            unary_message += ' 00 0'
        previous_byte = b

print(unary_message.strip())
