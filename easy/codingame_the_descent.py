# game loop
while True:
    target_p = 0
    target_h = 0

    # read the input and taget the highest mountain
    for i in range(8):
        mountain_h = int(input())  # represents the height of one mountain.

        if mountain_h > target_h:
            target_h = mountain_h
            target_p = i

    # the index of the mountain to fire on.
    print(target_p)
