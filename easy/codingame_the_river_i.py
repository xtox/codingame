# calculate the next point of a river
def next_point(n):
    result = n
    for i in str(n):
        result += int(i)

    return result


r_1 = int(input())
r_2 = int(input())

while True:
    if r_1 == r_2:
        meeting_point = r_1
        break

    # calculate the next point of the river depending on which one is the most advanced
    if r_1 > r_2:
        r_2 = next_point(r_2)
    else:
        r_1 = next_point(r_1)

print(meeting_point)
