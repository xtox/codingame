r = int(input())
v = int(input())

vault_duration = []
for i in range(v):
    c, n = [int(j) for j in input().split()]

    # calculate the duration to test all the combinations of a vault
    vault_duration.append(10 ** n * 5 ** (c - n))

# calculate the duration of the work
total_duration = 0
while len(vault_duration) > r:
    min_duration = min([dur for dur in vault_duration[:r]])
    total_duration += min_duration

    for i in range(r - 1, -1, -1):
        vault_duration[i] -= min_duration
        if vault_duration[i] == 0:
            vault_duration.pop(i)

# add the remaining when there is as robbers than vaults
total_duration += max([dur for dur in vault_duration[:r]])

# print the result
print(total_duration)
