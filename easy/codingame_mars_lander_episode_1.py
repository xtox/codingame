# read the initialization input (no use after)
surface_n = int(input())
for i in range(surface_n):
    land_x, land_y = [int(j) for j in input().split()]

# game loop
while True:
    # h_speed: the horizontal speed (in m/s), can be negative.
    # v_speed: the vertical speed (in m/s), can be negative.
    # fuel: the quantity of remaining fuel in liters.
    # rotate: the rotation angle in degrees (-90 to 90).
    # power: the thrust power (0 to 4).
    x, y, h_speed, v_speed, fuel, rotate, power = [int(i) for i in input().split()]

    # adapt the power to keep the vertical speed close but greater than -40 m/s (with a little margin)
    if v_speed < -39 and power < 4:
        power += 1
    elif v_speed >= -39 and power > 0:
        power -= 1

    # display the result
    print("0 " + str(power))
