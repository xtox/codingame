# clean all the characters except the brackets from the input()
brackets_str = ''.join([e for e in input() if e in '([{}])'])

# delete pairs of brackets
for i in range(len(brackets_str)//2):
    brackets_str = brackets_str.replace('()', '').replace('[]', '').replace('{}', '')

# print the result
print('true' if brackets_str == '' else 'false')