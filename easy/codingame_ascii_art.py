# read the input
l = int(input())
h = int(input())
t = input()

# write the text in ASCII Art
for i in range(h):
    row = input()
    result_line = ''

    # iterate on each letter of the text
    for c in t:
        # determine which letter should be display
        index = ord(c.upper()) - ord('A')

        if index < 0 or index > 26:
            index = 26

        # aggregate the result line
        result_line += row[index*l: (index+1)*l]
    
    # add the result line to the ASCII Art table
    print(result_line)
