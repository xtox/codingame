n = int(input())  # number of elements which make up the association table
q = int(input())  # number of file names to be analyzed

# mime_dict = extension:mime
mime_dict = dict(input().split() for i in range(n))
mime_dict_case_insens = {k.lower(): v for k, v in mime_dict.items()}

for i in range(q):
    fname = input().lower()  # one file name per line

    mime = ''
    # find the MIME associated to the extension
    if '.' in fname:
        mime = mime_dict_case_insens.get(fname.split('.')[-1])

    # if extension is not find in the dictionnary or not set, put UNKNOWN
    if not mime:
        mime = 'UNKNOWN'

    # print the result
    print(mime)
