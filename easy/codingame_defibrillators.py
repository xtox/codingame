import sys
import math

lon_user = float(input().replace(',', '.'))
lat_user = float(input().replace(',', '.'))
n = int(input())
d_min = sys.maxsize
name_closest_defib = ''

for i in range(n):
    # read the defibrilators data
    id_defib, name_defib, address_defib, phone_defib, lon_defib, lat_defib = input().split(';')

    # convert the defibrilator latitude and longitude
    lon_defib = float(lon_defib.replace(',', '.'))
    lat_defib = float(lat_defib.replace(',', '.'))

    # calculate the distance
    x = (math.radians(lon_defib) - math.radians(lon_user)) * math.cos((math.radians(lon_defib) + math.radians(lon_user)) / 2)
    y = math.radians(lat_defib) - math.radians(lat_user)
    d = math.sqrt(x**2 + y**2) * 6371

    # compare the distance to the distance min
    if d < d_min:
        d_min = d
        name_closest_defib = name_defib

# print the closest defibrilator
print(name_closest_defib)
