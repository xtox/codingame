# read the number of temperatures
n = int(input())

# read the list of temperatures
temperatures = [int(t) for t in input().split()]

# initialize the temperature closest to zero by max temperature
t_closest_zero = 5526

# if no temperature in the list set the result to zero
if n == 0:
    t_closest_zero = 0
# iterate on each temperature and keep the closest to zero
else:
    for t in temperatures:
        if abs(t_closest_zero) > abs(t) or (abs(t_closest_zero) == abs(t) and t > 0):
            t_closest_zero = t

print(t_closest_zero)
